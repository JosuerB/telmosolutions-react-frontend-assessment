<h1>Telmo Solutions </h1>

<h2>React Developer Assessment (FRONTEND)</h2>

**Note:** Please <span style="text-decoration:underline;">read carefully</span> and thoroughly follow the instructions written below.  Every tiny detail of work you have completed is appreciated.

<h2>Instructions</h2>


Create a simple app that has 3 components (including the parent component) and  requires users to populate a form and display the results after submission. 

The first component must have a form that has the following fields:



1. First Name
2. Middle Initial
3. Last Name
4. Contact Number
5. Email
6. Password
7. Birthday
8. Age
9. Profile Image

The second component should display all of the fields.

**Rules:**



* First Name, Last Name and Middle Initial properties must be grouped in a single object named “name” - a property of form object.
* Other fields should also be properties of the form object.
* Property names must be in [snake case](https://en.wikipedia.org/wiki/Snake_case).
* Form fields must be validated before submitting.
* Age field must be read-only and automatically calculated when birthday is applied.
* Avoid passing the form as property in any of the components. Use **context/reducer** hooks or any other **state management** instead.
* The second component must be a protected route. Navigating to the second component’s route will automatically redirect to the first page if the form is incomplete.
* Using CSS packages (Tailwind, Bootstrap, MUI etc..) to improve the UI of the app is appreciated.

<h2>TECHNOLOGY STACK</h2>




* React JS/TS

Fork or clone this repository: [https://gitlab.com/kharl0008/telmosolutions-react-frontend-assessment](https://gitlab.com/kharl0008/telmosolutions-react-frontend-assessment)

Create a pull request and a branch name of this format: **lastname-frontend**

**NOTE:** you can also create your own repository if you can’t create a pull request. Name the project the same format as the branch above.

**<span style="text-decoration:underline;">Deadline: 1 DAY AFTER THE INTERVIEW</span>**

Good Luck!
